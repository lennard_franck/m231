# Leistungsbeurteilung Modul 231 (V1)

Die Schlussnote setzt sich wie folgt zusammen:
```
30% Leistungsbeurteilung 1 - schr. Prüfung
30% Leistungsbeurteilung 2 - schr. Prüfung 
40% Leistungsbeurteilung 3 - Persönliches Dossier
```

# LB1 - Schriftliche Leistungsbeurteilung (1)

|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | keine |
| Nicht erfüllen der Zulassungsbedingungen | N/A |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochenen
 Unterlagen. **Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben.** 

## Inhalt (Unverbindlich)
 - Grundlagen Datenschutz (Lehrmittel EDÖB)
 - Herausforderungen in der digitalen Welt
 - Schutzwürdigkeit von Daten im Internet (siehe [Kompetenzmatrix](../00_kompetenzband/))
   - A1G: Kann schützenswerte Daten erkennen.
   - A1E: Kann unterschiedliche Regelungen (zBps DSG, DSGVO, Schulordnung) vergleichen und kritisch hinterfragen.

<br>Unterlagen: *Präsentationen*, *GitLab Repository zum Modul*, *Unterlagen auf Teams*

# LB2 - Schriftliche Leistungsbeurteilung (2)

|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | keine |
| Nicht erfüllen der Zulassungsbedingungen | N/A |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochenen
 Unterlagen. Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben. 

## Inhalt (Unverbindlich)
 - Git
   - Kennt die Grundlagen von Git
   - Kennt die wichtigsten Befehle (git add, git push, git pull, git commit, git status, git clone, git checkout)
 - Passwörter (siehe [Kompetenzmatrix](../00_kompetenzband/))
   - E1G: Kann die Prinzipien der Passwortverwaltung und "gute" Passwörter erläutern.
   - Kennt den Unterschied zwischen Authentifizierung und Autorisierung
   - Kennt Methoden der Mehrfaktor-Authentifizierung

<br>Unterlagen: *Präsentationen*, *GitLab Repository zum Modul*, *Unterlagen auf Teams*

## Persönliche Zusammenfassung für schriftliche Lernkontrollen
 - Umfang:
   - Ausgedruckt: Max. 2 x A4 Papier (=4 Seiten)
   - Handgeschrieben: Max 4 x A4 Papier (=8 Seiten)
 - Individuelle und selbstgeschriebene Zusammenfassung
 - Keine „Collagen“: Aus den Unterlagen 1:1 übernommenen Text oder
Seitenausschnitte sind nicht zugelassen.
   - Ausschnitte von einzelnen Grafiken / Schemas ist erlaubt.
 - Mindestumfang: Wichtigste Punkte jedes prüfungsrelevanten Themas
 - Zusammenfassung muss mit der Prüfung abgegeben werden!

# LB3 - Persönliches Dossier

 - **Informieren Sie sich über die Abgabetermine bei der Lehrperson!**

| N° | Kriterien                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Max  |
|----|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|
| 1  | Github oder Gitlab Account angelegt (1P), Repository für eigenes Lernportfolio angelegt (1P), Name des Repository bezieht sich auf den Inhalt und ist nachvollziehbar (1P), Repository der Lehrperson freigeben (1P)<br/> (Abgabedatum siehe Einstiegspräsentation (PDF)), Das persönliche Repository enthält ein README.md (1P)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | 5 P  |
| 2  | Passwortverwaltungsprogramm ausgewählt und Entscheid schriftlich begründet (Vergleich mit anderer Software, Vor und Nachteile) (1P), Entscheid mit Lehrperson besprochen (1P)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | 2 P  |
| 3  | Passwortverwaltungsprogramm (,dass die Mindestanforderungen erfüllt,) auf eigenen Gerät installiert und der Lehrperson gezeigt. (Abgabedatum siehe Einstiegspräsentation (PDF))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 1 P  |
| 4  | Zwei Checklisten (pro Checkliste 1P) des Datenschutzbeauftragten des Kanton Zürichs bearbeitet und gemäss Aufgabenbeschreibung im Git-Repository im Markdown-Syntax dokumentiert.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 2 P  |
| 5  | Ausgefülltes Auskunftsbegehren an Lehrperson geschickt (OHNE ID KOPIE!)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 1 P  |
| 6  | Eine mit PGP verschlüsselte E-Mail an die Lehrperson geschickt.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | 1 P  |
| 7  | Qualität und persönlicher Einsatz. Der/die Lernende überzeugt mit Einsatz, Fleiss und Können die Lehrperson.<br/>Der/die Lernende überzeugt mit Einsatz, Fleiss und Können die Lehrperson. In den Unterlagen des Lernenden ist ersichtlich, dass er/sie sich ins "Zeug gelegt hat", grosse und kleine Hürde mit Hartnäckigkeit und Geduld überwunden hat. Im Endprodukt sind die zu erreichenden Kompetenzen ersichtlich. Die persönliche Lernentwicklung ist beim Lernenden eindeutig erkennbar (Vergleich zum Vorwissen, Wissenszuwachs). Der/die Lernende hat sich bei Schwierigkeiten frühzeitig Hilfe gesucht. <br/>4 Punkte = sehr gut: Brillante Arbeiten, grosser Einsatz<br/>3 Punkte = gut: Tolle Arbeit, Einsatz ausreichend<br/>2 Punkte = ok: Beim Einsatz oder bei der Arbeit liegt noch mehr Potential drin<br/>1 Punkt = Naja, hat etwas gemacht. Einsatz fast nicht erkennbar. <br/>0 Punkte = Praktisch nichts gemacht. Einsatz unzureichend. | 4 P  |
|    | **Total**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | 16 P |
