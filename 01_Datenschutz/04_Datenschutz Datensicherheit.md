# Datensicherheit und Datenschutz 

Der Unterschied zwischen Datenschutz und Datensicherheit ist vielen nicht bewusst. In diesem Abschnitt lernen Sie den Unterschied zwischen den beiden Begriffen kennen. 

**Ziel: Kennt den Unterschied von Datenschutz und Datensicherheit.**

![Datenschutz und Datensicherheit](media/DatenschutzDatensicherheit.PNG)

Suchen wir auf einer bekannten Online Suchmaschine den Begriff "Datensicherheit" bekommen wir folgende Definition: "Datensicherheit verfolgt also das Ziel, Daten jeglicher Art gegen Bedrohungen, Manipulation, unberechtigten Zugriff oder Kenntnisnahme abzusichern."

Beim Begriff Datensicherheit geht um organisatorische Massnahmen, um die Datensicherheit zu gewährleisten. Die Hauptziele (vergl. *CIA triad*) sind:
 - **Verfügbarkeit** (Availability): Verhinderung von Systemausfällen, der Zugriff auf Daten muss innerhalb eines vereinbarten Zeitraumes gewährleistet sein
 - **Vertraulichkeit** (Confidentiality): Daten dürfen lediglich von autorisierten Benutzern gelesen bzw. modifiziert werden. Dies gilt sowohl beim Zugriff auf gespeicherte Daten, wie auch während der Datenübertragung
 - **Integrität** (Availability): Daten dürfen nicht unbemerkt verändert werden. Alle Änderungen müssen nachvollziebar sein. 

![Datensicherheit Dreieck](media/DatensicherheitDreieck.PNG)

*Das Dreieck der Datensicherheit bzw. Informationssicherheit mit den drei Schutzzielen.*

Bei der Suche des Begriffes "Datenschutz" erhalten wir folgende Defintion: "Je nach Betrachtungsweise wird Datenschutz als Schutz vor missbräuchlicher Datenverarbeitung, Schutz des Rechts auf informationelle Selbstbestimmung, Schutz des Persönlichkeitsrechts bei der Datenverarbeitung und auch Schutz der Privatsphäre verstanden."

Wenn wir auf [ChatGPT](https://openai.com/blog/chatgpt/) die Frage stellen, was der Unterschied zwischen Datensicherheit und Datenschutz ist, erhalten wir folgende Antwort:

![OpenAI Chat](media/openai_result_datensicherheit.PNG)

# Übung - Unterscheidung zwischen Datenschutz und Datensicherheit

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Quiz |
| Zeitbudget  |  10 Minuten |
| Ziel | Kann zwischen Datenschutz und Datensicherheit unterscheiden |

Lösen Sie das Quiz auf https://forms.office.com/e/dSuWeBpTKW

Die Antworten werden nach Abschluss des Quiz angezeigt. 

# Übung - Organisatorische Massnahmen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit: 3 bis 4 Personen |
| Aufgabenstellung  | Schuttziele der Datensicherheit verstehen |
| Zeitbudget  |  1/2 Lektion |
| Ziel | Jeder Lernende kann Massnahmen zu den drei Schutzzielen nennen und diese einordnen.  |

Notieren Sie zu dritt oder zu viert mindestens sechs organisatorische Massnahmen und verorten diese im Informationssicherheit bzw. Datensicherheitsdreieck. 

Halten Sie Ihre Massnahmen schriftlich fest (Poster, Präsentation), sodass Sie diese anschliessend im Plenum vorstellen können. 

# Quellen
 - https://www.heise.de/tipps-tricks/Datenschutz-vs-Datensicherheit-der-Unterschied-7158523.html
 - https://de.wikipedia.org/wiki/Informationssicherheit
 - https://linutronix.de/dienstleistung/iot_dds.php
 - https://www.fortinet.com/resources/cyberglossary/cia-triad
 - https://de.wikipedia.org/wiki/Datenschutz
 - https://blog.netwrix.de/2020/07/25/schutzziele-in-der-informationssicherheit-und-ihre-umsetzung-in-der-praxis/