# Authentifizierung mit Passwörter <!-- omit in toc -->

# Einleitung
Die Identifizierung mit Benutzername und Passwort ist die am häufigsten verwendete Art, um Zugang zu einer geschützten Ressource zu erhalten. Während der Benutzername meist eine öffentlich bekannte E-Mail Adresse ist oder vom Namen der Person abgeleitet wird, ist das Passwort eine zufällig ausgewählte Folge von Zeichen (theoretisch). Schaut man auf dem GitHub Repository [SecList](https://github.com/danielmiessler/SecLists/blob/master/Passwords/xato-net-10-million-passwords-10.txt), welche aus verschiedenen Quellen die am häufigsten verwendeten Passwörter zusammengestellt hat, sieht man, dass "123456", "password" oder "dragon" zu den häufigsten Passwörtern gehören. Daraus lässt sich schliessen, dass die User ihre Passwörter nicht rein zufällig wählen, sondern die Tendenz haben, einfache Kombinationen zu wählen.

Um nachzuvollziehen, weshalb es Passwörter überhaupt braucht, muss man den Begriff der Identifizierung zuerst verstehen. Der "Projektbericht Sichere Digitale Identitäten", der vom DIN (Deutschen Institut für Normung) und der DKE (Deutschen Kommission Elektrotechnik) herausgegeben wurde, beschreibt diesen Begriff ganz allgemein (S.87).

<blockquote>
Ausgangspunkt einer digitalen Identität ist zunächst eine Entität (Kap. 3.2.1), also eine existierende konkrete oder abstrakte Sache, für die eine digitale Identität geschaffen bzw. erzeugt wird. Dies geschieht dadurch,  dass  zunächst  gewisse  Attribute  der  Entität  festgestellt  (Kap.  3.2.2)  ggf.  festgelegt  und  in  einem  digitalen  Datensatz  abgebildet  (Kap.  3.2.3)  werden.  Dann  wird  die  digitale  Identität  eingerichtet,  also  die  Entität  mit  der  digitalen  Identität  (dem  Datensatz  mit  der  Sammlung  von  Attributen)  verbunden  (Kap 3.2.4). Dies geschieht bspw. durch ein eineindeutiges Merkmal, welches die Entität besitzt oder welches auf sie aufgebracht oder integriert wird (Kap. 3.2.4.1). „Eineindeutig“ bedeutet dabei, dass ein Attributsatz nur für diese eine Entität existiert -  also einmalig ist. Nun wird der digitale Datensatz gespeichert und u.a. beim „Nutzen“ der Entität gepflegt, aktualisiert bzw. verwaltet (Kap. 3.2.5). Wird die Entität in einem Prozess in irgendeiner  Weise  genutzt,  kann  durch  das  eineindeutige  Merkmal  die  digitale  Identität  aufgerufen  und  bspw.  zur  Verifikation  genutzt  werden  (Kap.  3.2.6).  Dies  geschieht  so  lange  bis  die  Entität  bspw.  unersetzt  vernichtet  wird  und  ein  Aufbewahren  der  digitalen  Identität  ggf.  nicht  mehr  notwendig  ist.  Je  nach  Anwendungsfall wird die digitale Identität dann gelöscht oder gesperrt und archiviert.

In  einem  einfachen  Beispiel  bedeutet  dies  (vgl.  Abbildung  21):  Eine  Person  packt  ein  Paket  und  möchte  dies  verschicken.  Das  Paket  ist  die  Entität  (Kap.  3.2.1)  für  die  in  der  Postfiliale  eine  Digitale  Identität  geschaffen  wird.  Der  Postangestellte  stellt  fest  (Kap.  3.2.2),  dass  das  Paket  20  ×  20  cm  groß  ist,  400  g  schwer und erfragt die Empfängerdaten. Er gibt die Daten in sein System ein (schafft also ein Abbild, Kap. 3.2.3)  und  erstellt  eine  eineindeutige  Paketnummer,  die  per  Strichcode-Aufkleber  auf  das  Paket  geklebt  (eingerichtet,  Kap.  3.2.4)  wird.  Die  digitale  Identität  wird  in  dem  weltweit  zugreifbaren  System  der  Post  gespeichert  (verwaltet,  Kap.  3.2.5)  und  jedes  Mal,  wenn  das  Paket  bei  Zwischenstationen  ankommt,  wird  der  Code  eingescannt  und  somit  festgestellt,  welches  Paket  es  ist  und  welche  Bestimmung  es  hat  (Kap. 3.2.6). Der Ort wird in der Digitalen Identität aktualisiert (Nutzen, Kap. 3.2.6 und Verwalten, Kap. 3.2.5) und wenn das Paket zugestellt wurde, kann diese Digitale Identität auch wieder gelöscht werden (Kap. 3.2.7). 
</blockquote>

Das obige Beispiel des Projektberichtes beschreibt, wie sich ein Paket mithilfe des Strichcodes gegenüber den Zwischenstationen (z.B. eine automatische Paket-Sortieranlage oder der Scan-Gerät des Postboten). Der Strichcode ist dabei sichtbar aufgeklebt und es gibt keine Vorkehrungen, um missbrauch zu verhindern. Käme ein Paket auf die Idee sich als ein anderes Paket auszugeben, weil es dessen Nummer im Transportlastwagen abgelesen hat, könnte es sich zu einem anderen Ort transportieren lassen. Vermutlich würde das System einen Alarm an das Monitoring machen, weil ein Paket plötzlich andere Masse hat oder weil diesselbe Paket-Nummer bereits erfasst wurde. 

<blockquote>
Einrichten der besonderen physischen Entität - Mensch 
Der Mensch ist eine besondere Form der Entität. Durch seine grundsätzlich vorhandenen unterschiedlichen Fähigkeiten ist er ein Sonderfall, auch wenn diese Fähigkeiten durchaus mit den Möglichkeiten bei Dingen vergleichbar sind. Der Mensch hat in jedem Fall eigene Sicherheitsmerkmale im Sinne  der  Biometrie  oder  des  typischen  Verhaltens.  Zudem  kann  er  Dinge  sicher  speichern  (Passwort,  Gestenmuster, etc.). Er kann Situationen erkennen und analysieren, kann aktiv kommunizieren. Der Mensch kann aber keine höheren kryptographischen Prozesse vornehmen oder größere Datenmengen so speichern, dass sie für die IT unmittelbar verwendbar sind. Er kann aber Träger von Datenspeichern, Secure Elements  und  Hochsicherheitsmodulen  verwahren.  In  diesem  Sinne  ist  der  Mensch  prädestiniert  für  die  Kombination  mit  weiteren  Authentifikationsmitteln.  Bspw.  können  RFID-Chip  oder  Hochsicherheitsmodul  in Form einer Chipkarte für die Verifikation der Entität Mensch genutzt werden (z.B. Bankkarten, elektronischer  Personalausweis).  Wenn  die  Daten  auf  der  Chipkarte  oder  die  digitale  Identität  dahinter  auch  noch  Informationen  über  die  Biometrie  der  Person  beinhalten,  kann  diese  zur  Verifikation  der  Verbindung  Mensch  und  Chipkarte  dienen.  Das  meistgenutzte  Verfahren  hierbei  ist,  dass  der  Mensch  ein  Sicherheitsmerkmal“integriert“  mit  dem  er  die  Verifikation  der  Verbindung  zwischen  Chipkarte  und  Mensch vornehmen kann: das Passwort.  Bei der Entität Mensch werden bspw. folgende die Authentifizierungsmöglichkeiten unterschieden: 
• per Wissen aufgeteilt in Erinnern (Passwort, Unterschrift, Gestenmuster o.ä.) oder Erkennen 
(Geheimnis auf Bildern erkennen o.ä.),  
• per Besitz (Chipkarte)  
• per Biometrie 
</blockquote>

Die grosse Herausforderung bei der "Entität Mensch" ist, dass ein Missbrauchspotential vorhanden ist. Konkret bedeutet das, dass eine Entität versucht, sich als eine andere Entität gegenüber einer dritten Stelle zu authentifizieren. Einfacher ausgedrückt: Eine unberechtigte Person versucht in ein fremdes System einzudringen. 

Weiter Erfahren wir aus dem Abschnitt "Einrichten der besonderen physischen Entität - Mensch", dass eine Unterscheidung zwischen den Authentifizierungsmöglichkeiten gemacht wird:
 - per Wissen aufgeteilt in Erinnern (Something you know)
 - per Besitz (Something you have)  
 - per Biometrie (Something you are)

Unter einer Mehrfaktor-Authentifizierung versteht man eine Authentifizierung, die unterschiedliche Authentifizierungsmöglichkeiten nutzt. Eine häufig verwendete Methode ist auch das Zuschicken eines Codes an das Mobiltelefon des Benutzers (mTAN), welches gerne von den Banken verwendet wurde, aber immer mehr durch andere Verfahren ersetzt wird. 

In diesem Teil des Moduls 231 werden Sie sich mit dem Thema der Passwörter beschäftigen und die Themen "Was ist ein sicheres Passwort" und "Passwort-Verwaltung" selbstständig erarbeiten. 

# Übungen

## Einzelarbeit
 - [Authentifizierung und Autorisierung](03_Authentifizierung%20und%20Autorisierung.md)
 - [Have I been pwned](04_Have%20I%20been%20pwned.md)
 - [Multi-Faktor-Authentifizierung](05_MFA.md)
 - [Passwortmanager](06_Passwortmanager.md)

## Gruppenarbeit
 - [Diskussion Thema Passwörter](02_Diskussion%20Passwörter.md)

# Bezug zur Modulidentifikation
## 1.1. Handlungsziele
 - **2.** Überprüft und verbessert gegebenenfalls die Datensicherheit der eigenen Infra-struktur.

## 1.2. Handlungsnotwendige Kenntnisse
 - **2.3.** Kennt Techniken des Zugriffsschutzes, Passwortmanager und Prinzipien der Passwortverwaltung.
 - **2.4** Kennt den Unterschied von Authentifizierung und Autorisierung.

## 1.3. Kompetenzen
 - **E1G:** Kann die Prinzipen der Passwortverwaltung und "gute" Passwörter erläutern.
 - **E1F:** Kann einen Passwort Manager einsetzen.
 - **E1E:** Kennt und setzt erweiterte Möglichkeiten der Authentifizierung ein. Kennt Vor- und Nachteile von Passwortmanagern.



# Quellen
 - https://lock.cmpxchg8b.com/passmgrs.html
 - https://anonymousplanet.org/guide.html#appendix-a2-guidelines-for-passwords-and-passphrases
 -  https://github.com/danielmiessler/SecLists
 - Din https://www.din.de/resource/blob/306552/1e281ee0a725f5569469af8285ff0183/din-dke-projektbericht-data.pdf
 - https://www.netsec.news/summary-of-the-nist-password-recommendations-for-2021/
