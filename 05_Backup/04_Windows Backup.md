# Teil 1: Windows Backup einrichten - Offline
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie haben das Standard Windows Backup auf Ihren Notebook eingerichtet |

"Dateiversionsverlauf" oder "File History" ist das integrierte *file-level based* Backup unter Windows. Es erlaubt Ihnen ohne die Installation von zusätzlicher Software von Ihren persönlichen Dateien eine Datensicherung zu erstellen. 

## Aufgabenstellung
Wenn Sie ein Windows Notebook in der Schule verwenden: Nutzen Sie diese Anleitung und richten Sie sich ein Backup ein. https://support.microsoft.com/en-us/windows/file-history-in-windows-5de0e203-ebae-05ab-db85-d5aa0a199255

# Teil 2: Windows Backup einrichten - Online
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie haben OneDrive Backup eingerichtet |

Microsoft möchte möglichst viele Kunden auf Ihre OneDrive Plattform bringen und haben (vermutlich unter anderem) deshalb OneDrive als Backup Methode gleich in Windows 10 integriert. Sie haben ja bereits gelernt, dass die Verwendung von Cloudbasierten Services nicht ohne Risiken sind. 

Informieren Sie sich über das Tool: 
https://support.microsoft.com/de-de/office/aktivieren-von-onedrive-backup-4e44ceab-bcdf-4d17-9ae0-6f00f6080adb