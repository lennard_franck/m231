# Backup <!-- omit in toc -->

<b>Kein Backup - Kein Mitleid</b>

Backups manuell zu erstellen oder ein automatisches Backup zu konfigurieren gehört zu den Tätigkeiten, die keinen unmittelbaren Vorteil bringen: Wenn man die Fotos der letzten Sommerferien vom Laptop auf eine externe Festplatte kopiert und diese anschliessend sicher verstaut, dann bringt das im Moment keinen Benefit. Weil es keinen unmittelbaren Vorteil bringt und aufwändig ist, wird das Backup häufig vernachlässigt. Nicht selten benötigt es ein einschneidendes Erlebnis bis man die Notwendigkeit von Backups einsieht und man sich zuverlässig darum kümmert. Während der Verlust aller Familienfotos, des Gaming-Spielstandes oder einer grösseren Schularbeit bereits sehr schmerzhaft und traumatisierend sein kann, muss bei einem Datenverlust in einer Firma schnell mit einem Sachschaden von mehreren hunderttausend Franken gerechnet werden. 

**Eine Definition des Begriffes "Backup":**

```Ein Backup ist die räumliche Trennung der Orginaldatenträger und der Backup Medien.```

In Rahmen dieses Moduls schauen wir uns die Grundlagen von Backups an und fokussieren uns auf das Backup der persönlichen Daten. Wie man ein Backup für einen Server einrichtet und weiterführende Themen erlernen Sie in fortführenden Modulen zu einem späteren Zeitpunkt in ihrer Lehrzeit. 

Für das persönliche Backup dienen folgende Grundsätze:
 - Neuen Laptop erhalten: Als erstes Backup einrichten! (Nicht verschieben! Hohes Risiko das zu Vergessen: Aus einem *später* wird schnell ein *nie*)
 - Neues Handy einrichten: Backup konfigurieren!<br/>(Meistens erfolgt ein automatisches Backup auf Google Drive oder iCloud **Achtung: Datenschutzaspekte bedenken!**)
 - 8 Stunden an einem Dokument gearbeitet, aber die Backup-HDD für das Notebook nicht dabei und lieber keine Cloud verwenden: Alle Dateien mit [7z](https://www.7-zip.org/) Zippen, mit Passwort versehen und auf einen Filehosting-Dienst laden. 
 - Sie müssen gerade ein von ihnen geschriebenes Programm überarbeiten. Git-Repo eingerichtet, alles Commited und gepushed?
 - Gerade einen Switch am Konfigurieren: Nach jeder grösseren Änderung kurz einen Config Export machen: Wenn man die Konfiguration kaputt macht, kann man wieder auf die funktionierende Version zurück. 
 - Firmwareupdate auf einer Firewall: Vorher ein Backup der Konfiguration machen.
 - usw.

Damit Sie sich möglichst schnell daran gewöhnen **immer** Backups zu machen gilt an der TBZ deshalb die Regel "**Kein Backup - Kein Mitleid**". Das bedeutet: Wenn der Laptop, auf dem zum Beispiel die aktuellste Version ihres Portfolios ist, gestohlen wird, Sie ihn verlieren oder er kaputt geht, dann gibt es kein Verständnis oder Nachfristen. Im schlimmsten Fall kann das die Note 1 bedeuten. Unserer konsequenten Haltung erleichtert es Ihnen sich an das Backup machen zu gewöhnen. Am Schluss ihrer Lehrzeit sollte *Backupen* so selbstverständlich sein wie Zähneputzen. Wenn Während der Verlust einer Schularbeit verkraftbar ist, kann der Verlust von wichtigen Geschäftsdaten zu Sach- und Personenschäden führen. 

Denn es gilt *Murphys law: "Anything that can go wrong will go wrong."* ([Wikipedia](https://de.wikipedia.org/wiki/Murphys_Gesetz)). Die Frage ist nicht, **ob**, sondern **wann** Sie das Backup benötigen.

**Erfüllen Sie Ihre Sorgfaltspflicht und erstellen Sie IMMER und AUSNAHMSLOS Backups!**

https://www.storage-insider.de/was-ist-ein-backup-eine-datensicherung-a-621411/

Unter dem Begriff "Backup" fällt nicht nur das *Kopieren* von Daten auf andere Datenträger, sondern viel allgemeiner die Verfügbarkeit durch "Redundanz" (lat. redundare‚ 'im Überfluss vorhanden sein'): 
 - Ein zweites Smartphone, falls das erste kaputt geht
 - Ein zweite Kasse im Supermarkt, falls die erste kputt geht
 - Ein zweiter ITler auf Picketdienst, falls der erste ausfällt
 - Eine zweite Stromleitung ins Gebäude
 - Drei redundante Notstromgeneratoren
 - Die Kopie aller Daten auf *Google Drive*, falls *OneDrive* nicht erreichbar ist. 
 - usw.

Mit dem Backup wird im Datensicherheits-Dreieck das Schuttziel "Verfügbarkeit" gewährleistet. 

In dieser Modul liegt der Fokus auf dem persönlichen Backups. Die Organisation von Backups für Betriebe wird dann im Modul 143 Thema sein. 


# Übungen

## Einzelarbeiten
 - [01 Leseauftrag Thema Backup](01_Leseauftrag.md)
 - [03 Backupkonzept](03_Backupkonzept.md)
 - [04 Windows Backup](04_Windows%20Backup.md)
 - [05 Sicheres Cloud-Backup](05_Sicheres%20Cloud-Backup.md)
 - [06 Backup Termin setzen](06_Backup%20Termin.md)

## Gruppenarbeit
 - [02 Begriffe klären](02_Begriffe%20Klären.md)

# Ziele
 - B3G: Kann den Zweck, aber auch die Grenzen eines Backups erläutern.
 - B3F: Plant und macht regelmässig ein Backup seiner Daten.
 - B3E: Setzt ein Backupkonzept ein und überprüft es regelmässig auf Restorefähigkeit.

# Nützliche Tools
 - [Clonezilla](https://clonezilla.org/) ist ein eine Open Source Software für die Erstellung vom Images und Klonen von Festplatten. Sie erlaubt es 1:1 Backups von der ganzen Festplatte zu erstellen. 
 - [rsync](https://de.wikipedia.org/wiki/Rsync) Ein Leistungsstarkes Linux Werkzeug zum synchronisieren von Dateien oder Erstellung von Backups
 - [Acronis](https://www.acronis.com/de-de/) Softwarehersteller von leistungsstarker Backup Software
 - [Macrium](https://www.macrium.com/) Softwarehersteller von leistungsstarker Backup Software
 - [File History in Windows](https://support.microsoft.com/en-us/windows/file-history-in-windows-5de0e203-ebae-05ab-db85-d5aa0a199255) Standard Tool zum Erstellen von Dateilevel-Backups
 - [Time Machine (Apple)](https://de.wikipedia.org/wiki/Time_Machine_(Apple)) Dateisicherungssoftware von Apple
 - [Timeshift (Ubuntu)](https://github.com/teejee2008/timeshift) Dateisicherungssoftware für Linux Desktopsysteme
 - [Duplicati](https://www.duplicati.com/) / [Duplicacy](https://duplicacy.com/) Verschlüsseltes Backup in die Cloud

# Weiteres

 - [1 Std, C'T-Uplink, Podcast-Video über SSD/USB-Festplatten/USB-Sticks, Backup und Datenrettung](https://www.youtube.com/watch?v=bphSARufzj4&pp=ygUJY3QgdXBsaW5r), Sept 2023
